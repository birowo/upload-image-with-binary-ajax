package main

import (
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/fasthttp/router"
	"github.com/rs/xid"
	"github.com/valyala/fasthttp"
)

type Img struct {
	Name, Descript string
	LastModif      time.Time
}
type Imgs map[string]Img

var images = Imgs{}

type Form []byte

func (form Form) GetField(n int) (field []byte, ret Form) {
	var l int
	formLen := len(form)
	//field size
	if n > 4 && n < 13 { //if fixed size
		l = n - 4
		n = 0
	} else { //var size
		switch n {
		case 1: //max. 255 bytes
			if formLen > 0 {
				l = int(form[0]) + 1
			}
		case 2: //max. 65535 byte
			if formLen > 1 {
				l = int(form[1])<<8 + int(form[0]) + 2
			}
		case 3: //max. 16777215 byte
			if formLen > 2 {
				l = int(form[2])<<16 + int(form[1])<<8 + int(form[0]) + 3
			}
		default:
			return
		}
	}
	if l <= formLen {
		field = form[n:l]
		ret = form[l:]
	}
	return
}
func body(body []byte, ctnTyp string) fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {
		ctx.SetContentType(ctnTyp)
		ctx.SetBody(body)
	}
}
func BsToUInt64(bs []byte) uint64 {
	return uint64(bs[0]) + uint64(bs[1])<<8 + uint64(bs[2])<<16 + uint64(bs[3])<<24 + uint64(bs[4])<<32 + uint64(bs[5])<<40 + uint64(bs[6])<<48 + uint64(bs[7])<<56
}
func imgPost(ctx *fasthttp.RequestCtx) {
	var ids []string
	form := Form(ctx.Request.Body())
	for len(form) > 0 {
		var imgName, dscrpt, imgModBs, image []byte
		imgName, form = form.GetField(1)
		dscrpt, form = form.GetField(1)
		imgModBs, form = form.GetField(12)
		image, form = form.GetField(3)
		imgMod := time.UnixMilli(int64(BsToUInt64(imgModBs)))
		id := xid.New().String()
		ids = append(ids, id)
		images[id] = Img{string(imgName), string(dscrpt), imgMod} //simpan di map di contoh ini, tapi di aplikasi sesungguhnya simpan di database
		if err := os.WriteFile("upload/"+id, image, 0664); err != nil {
			println(err.Error())
			return
		}
	}
	fmt.Println(images)
	ctx.SetBodyString(strings.Join(ids, "|"))
}
func imgGet(ctx *fasthttp.RequestCtx) {
	id := ctx.UserValue("id").(string)
	body, err := os.ReadFile("upload/" + id)
	if err != nil {
		println(err.Error())
		return
	}
	ctnTyp := http.DetectContentType(body)
	fmt.Println(id, len(body), ctnTyp)
	ctx.SetContentType(ctnTyp)
	ctx.SetBody(body)
}
func main() {
	rootHtm, err := os.ReadFile("root.htm")
	if err != nil {
		println(err.Error())
		return
	}
	route := router.New()
	route.GET("/", body(rootHtm, "text/html; charset:utf-8"))
	route.POST("/img", imgPost)
	route.GET("/img/{id}", imgGet)
	fasthttp.ListenAndServe(":8080", route.Handler)
}
